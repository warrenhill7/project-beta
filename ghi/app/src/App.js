import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import TechnicianForm from './Services/TechnicianForm';
import ServiceAppointmentForm from './Services/ServiceAppointmentform';

import AppointmentsList from './Services/ListAppointments';
import DropdownMenu from './Services/DropDownMenu';




function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <DropdownMenu />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path='/technician/new' element= {<TechnicianForm />}/>

          <Route path='/service/new' element={<ServiceAppointmentForm service={props.service}/>}/>


          <Route path="models">


            </Route>
          <Route path='/appointments' element={<AppointmentsList appointments={props.appointments}/>}/>


        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
