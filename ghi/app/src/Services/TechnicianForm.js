import React, { useState, useEffect } from 'react';


const TechnicianForm = () => {
  const [technicians, setTechnicians] = useState([]);
  const [TechnicianName, setTechnicianName] = useState('');
  const [employeeNumber, setEmployeeNumber] = useState('');


 useEffect(() => {
  const fetchTechnicians = async () => {
    const response = await fetch('http://localhost:8080/api/technicians/');
    const data = await response.json();
    setTechnicians(data.technicians)
  };
  fetchTechnicians();
 },[])

const handleTechnicianChange = event => {
  setTechnicianName(event.target.value);
};

const handleEmployeeNumberChange = event => {
  setEmployeeNumber(event.target.value);
};

const handleCreateTechnician = async () => {
  const response = await fetch('http://localhost:8080/api/technicians/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      name:TechnicianName,
      employee_number: employeeNumber,
    }),
  });
  const data = await response.json();
  console.log(data);
  setTechnicianName('');
  setEmployeeNumber('');
};

return (
  <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow =-4 mt-4">
        <h1>Enter A Technician</h1>
        <div className="form-floating mb-3">
            <input
              placeholder="Technician Name"
              required
              type="text"
              name="technician_name"
              id="technician_name"
              className="form-control"
              value={TechnicianName}
              onChange={handleTechnicianChange}
            />
            <label htmlFor="technician_name">Technician Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              placeholder="Employee Number"
              required
              type="text"
              name="employee_number"
              id="employee_number"
              className="form-control"
              value={employeeNumber}
              onChange={handleEmployeeNumberChange}
            />
            <label htmlFor="employee_number">Employee Number</label>
          </div>
          <button className="btn btn-primary" onClick={handleCreateTechnician}>
            Create
          </button>
        </div>
      </div>
    </div>
  );
};

export default TechnicianForm;
