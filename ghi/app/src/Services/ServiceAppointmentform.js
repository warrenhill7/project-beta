import React, { useState, useEffect } from 'react';


const ServiceAppointmentForm = () => {
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [vinNumber, setVinNumber] = useState('');
  const [ownerName, setOwnerName] = useState('');
  const [dateTime, setDateTime] = useState('');
  const [selectedTechnician, setSelectedTechnician] = useState([]);
  const [serviceReason, setServiceReason] = useState('');
  const [purchasedFromDealership, setPurchasedFromDealership] = useState('');

 useEffect(() => {
  const fetchServiceAppointment = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');
    const data = await response.json();
    setAppointments(data.appointments)
  };
  fetchServiceAppointment();
 },[])

 useEffect(() => {
  const fetchTechnicians = async () => {
    const response = await fetch('http://localhost:8080/api/technicians/');
    const data = await response.json();
    setTechnicians(data.technicians)
  };
  fetchTechnicians();
 },[])
const handleVinChange = event => {
  setVinNumber(event.target.value);
};

const handleOwnerNameChange = event => {
  setOwnerName(event.target.value);
};

const handleDateTimeChange = event => {
  setDateTime(event.target.value);
};

const handleSelectedTechnicianChange = event => {
  setSelectedTechnician(event.target.value);
};


const handleServiceReasonChange = event => {
  setServiceReason(event.target.value);
};

const handleCreateServiceAppointment = async () => {
  const response = await fetch('http://localhost:8080/api/appointments/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      vin:vinNumber,
      customer_name: ownerName,
      appointment_datetime: dateTime,
      reason_for_service: serviceReason,
      purchased_from_dealership: purchasedFromDealership,
      technician: selectedTechnician,
    }),
  });
  const data = await response.json();
  console.log(data);
  setVinNumber('');
  setDateTime('');
  setOwnerName('');
  setServiceReason('');
  setPurchasedFromDealership('');
  setSelectedTechnician('');
};
    return (
        <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Enter A Service Appointment</h1>
            <div className="form-floating mb-3">
              <input placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" value={vinNumber} onChange={handleVinChange} />
              <label htmlFor="vin">Vehicles VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="customer_name" required type="text" name="customer_name" id="customer_name" className="form-control" value={ownerName} onChange={handleOwnerNameChange} />
              <label htmlFor="customer_name">Vehicle Owner's Name</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="appointment_datetime" required type="date" name="appointment_datetime" id="appointment_datetime" className="form-control" value={dateTime} onChange={handleDateTimeChange} />
              <label htmlFor="appointment_date">Date & Time of Appointment</label>
            </div>
            <div className="form-floating mb-3">
          <select
          className="form-select"
          onChange={handleSelectedTechnicianChange}
          value={selectedTechnician}
          >
             <option value="">Select a technician</option>
              {technicians && technicians.map(technician => (
                <option key={technician.id} value={technician.id}>
                  {technician.name}
                </option>
              ))}
            </select>
            <label htmlFor="technician">Technician Name</label>
          </div>
            <div className="form-floating mb-3">
              <input placeholder="Reason for Service" required type="text" name="reason_for_service" id="reason_for_service" className="form-control" value={serviceReason} onChange={handleServiceReasonChange} />
              <label htmlFor="reason_for_service">Service Needed</label>
            </div>
            <button className="btn btn-primary" onClick={handleCreateServiceAppointment}>Create</button>

        </div>
      </div>
    </div>
    );
}

export default ServiceAppointmentForm;
