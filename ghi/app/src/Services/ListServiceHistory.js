import { useEffect, useState } from 'react';

function ListAppointments() {
    const [appointments, setAppointments] = useState([])

    const getData = async () => {
      const response = await fetch('http://localhost:8080/api/appointments/')
      if (response.ok) {
        const data = await response.json()
        console.log(data.appointments)
        setAppointments(data.appointments)
      }
    }

    useEffect(()=>{
        getData()
    }, [])

    const handleDelete = async (e) => {
      const url = `http://localhost:8080/api/delete/${e.target.id}`

      const fetchConfigs = {
          method: "GET",
          headers: {
              "Content-Type": "application/json"
          }
      }

      const resp = await fetch(url, fetchConfigs)
      const data = await resp.json()

      setAppointments(appointments.filter(appointment => String(appointment.id) !== e.target.id))
      // getData()
    }

    return (
        <table className="table table-striped">
          <thead>
            <tr>
                <th>VIN</th>
                <th>Customer Name</th>
                <th>Date & Time</th>
                <th>Technician</th>
                <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {appointments.map(appointment => {
              return(
                <tr key={appointment.href}>
                    <td key={appointment.vin}>{ appointment.vin }</td>
                    <td key={appointment.customer_name}>{ appointment.customer_name }</td>
                    <td key={appointment.appointment_datetime}>{ appointment.appointment_datetime }</td>
                    <td key={appointment.technician.name}>{ appointment.technician.name }</td>
                    <td key={appointment.reason_for_service}>{ appointment.reason_for_service }</td>
                    <td><button onClick={handleDelete} id={appointment.id} className="btn btn-danger">Cancel</button></td>
                    <td><button onClick={handleDelete} id={appointment.id} className="btn btn-success">Finished</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
    )
 }

export default ListAppointments;
