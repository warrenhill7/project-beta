import React, { useState } from "react";


const links = [
    { text: "home", url: "/"},
    { text: "About", url: "/about"},
    { text: "Contact", url: "/contact"},
];


function DropdownMenu() {
    const [isOpen, setIsOpen] = useState(false);


    function toggle() {
        setIsOpen(!isOpen);
    }

    return (
        <div className="dropdown">
        <button className="dropdown-toggle" onClick={toggle}>
          Links
        </button>
        <ul className={isOpen ? "dropdown-menu show" : "dropdown-menu"}>
          {links.map((link) => (
            <li key={link.url}>
              <a href={link.url}>{link.text}</a>
            </li>
          ))}
        </ul>
      </div>
    );
}

export default DropdownMenu;
