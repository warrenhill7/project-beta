import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
            <li className="nav-item">
              <NavLink className="nav-link" to="technician/new">Enter a Technician</NavLink>
              <NavLink className="nav-link" to="service/new">Enter a service Appointment</NavLink>
              <NavLink className="nav-link" to="appointments">List Of Appointments</NavLink>
            </li>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
