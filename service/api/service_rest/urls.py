from django.urls import path

from .views import appointment_list, list_technician, appointment_detail

urlpatterns = [
    path("appointments/", appointment_list, name="appointment_list"),
    path("technicians/", list_technician, name="create_technician"),
    path("delete/<int:id>/", appointment_detail, name="delete_appointment")

]
