from django.db import models

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, null=True, unique=True)
    vin = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(null=True)


class Appointment(models.Model):
    vin = models.CharField(max_length=25)
    customer_name = models.CharField(max_length=255)
    appointment_datetime = models.DateTimeField()
    reason_for_service = models.TextField()
    purchased_from_dealership = models.BooleanField(default=False)
    technician = models.ForeignKey(
        'Technician',
        related_name="technician",
        on_delete=models.CASCADE,
    )
