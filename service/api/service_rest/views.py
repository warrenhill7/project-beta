from django.shortcuts import render
from .models import AutomobileVO, Appointment, Technician
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import AutomobileVOEncoder, AppointmentDetailEncoder, AppointmentListEncoder, TechnicianEncoder


@require_http_methods(["GET","POST"])
def appointment_list(request, vin_id=None):
    if request.method == "GET":
        if vin_id is not None:
            new_apt = Appointment.objects.filter(id=vin_id)
        else:
            new_apt = Appointment.objects.all()
        return JsonResponse(
            {"appointments": new_apt},
            encoder=AppointmentListEncoder,
            safe=False
        )


    else:
        content = json.loads(request.body)
        print(content)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
            # if the vin number is in the database then VIP == True
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=404,
            )
        try:
            vin = AutomobileVO.objects.get(vin=content["vin"])
            if vin:
                vip = True

        except AutomobileVO.DoesNotExist:
            vip = False
        if vip:
            content["purchased_from_dealership"] = True
        else:
            content["purchased_from_dealership"] = False


        try:
            new_appointment = Appointment.objects.create(**content)
            return JsonResponse(
            new_appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Vin Id"},
                status=404
            )


@require_http_methods(["GET", "DELETE"])
def appointment_detail(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"]) #creating a hat
def list_technician(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            technicians = Technician.objects.filter(technician=automobile_vo_id)
        else:
            technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )


    else: # THIS IS THE POST REQUEST
        content = json.loads(request.body)
        print("CONTENT LABLE", content)

        try:
            tech = Technician.objects.create(**content)
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=404,
            )
