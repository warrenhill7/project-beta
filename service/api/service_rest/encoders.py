from .models import AutomobileVO, Appointment, Technician
from common.json import ModelEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties=['vin', 'import_href']


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "appointment_datetime",
        "reason_for_service",
        "purchased_from_dealership",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["customer_name",
                  "appointment_datetime",
                  "vin",
                  "reason_for_service",
                  "purchased_from_dealership",
                  "technician",
                  "id",
                  ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
