from django.urls import path
from .views import list_salespeople, list_customers, list_sales_records, filter_sales

urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("customers/", list_customers, name="list_customers"),
    path("salesrecords/", list_sales_records, name="list_sales_records"),
    path("salesrecords/<int:sales_person_id>/", filter_sales, name="filter_sales"),
]
