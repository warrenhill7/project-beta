# Generated by Django 4.0.3 on 2023-03-09 20:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0006_recordsale_automobile_recordsale_customer_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recordsale',
            name='automobile',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='record_sales', to='sales_rest.automobilevo'),
        ),
    ]
