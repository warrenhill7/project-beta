# Generated by Django 4.0.3 on 2023-03-09 01:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_automobilevo'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=70)),
                ('address', models.CharField(max_length=70)),
                ('phone_number', models.CharField(max_length=15)),
            ],
        ),
        migrations.DeleteModel(
            name='AutomobileVO',
        ),
    ]
