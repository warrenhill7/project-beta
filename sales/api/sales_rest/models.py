from django.db import models
from django.urls import reverse

class SalesPerson(models.Model):
    name = models.CharField(max_length=70)
    employee_number = models.SmallIntegerField(unique=True)


class Customer(models.Model):
    name = models.CharField(max_length=70)
    address = models.CharField(max_length=70)
    phone_number = models.CharField(max_length=15)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class RecordSale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="record_sales",
        on_delete=models.CASCADE,
        null=True,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="record_sales",
        on_delete=models.CASCADE,
        null=True,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="record_sales",
        on_delete=models.CASCADE,
        null=True,
    )
    price = models.IntegerField()
