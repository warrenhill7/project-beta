from common.json import ModelEncoder
from .models import SalesPerson, Customer, RecordSale, AutomobileVO

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number"]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number"]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]

class RecordSaleListEncoder(ModelEncoder):
    model = RecordSale
    properties = [
        "price"
        ]

    def get_extra_data(self, o):
        return {
            "automobile": {"vin": o.automobile.vin},
            "sales_person": {"name": o.sales_person.name,
                             "employee_number": o.sales_person.employee_number},
            "customer": {"name": o.customer.name}
        }
    encoders = {"automobile": AutomobileVOEncoder}
