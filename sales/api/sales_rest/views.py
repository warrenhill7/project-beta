from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import SalesPerson, Customer, RecordSale, AutomobileVO
from .encoders import SalesPersonListEncoder, CustomerListEncoder, RecordSaleListEncoder

@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalesPersonListEncoder,
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_sales_records(request):
    if request.method == "GET":
        record_sales = RecordSale.objects.all()
        return JsonResponse(
            {"record_sales": record_sales},
            encoder=RecordSaleListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
            {"message": "automobile not working!!!!!!"}
            )
        try:
            salesperson_name = content["sales_person"]
            sales_person = SalesPerson.objects.get(name=salesperson_name)
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            return JsonResponse(
            {"message": "salesperson not working!!!"}
            )
        try:
            customer_name = content["customer"]
            customer = Customer.objects.get(name=customer_name)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
            {"message": "customer not working!!!"}
            )
        record_sales= RecordSale.objects.create(**content)
        return JsonResponse(
            record_sales,
            encoder=RecordSaleListEncoder,
            safe=False,
        )

@require_http_methods(["GET"])
def filter_sales(request, sales_person_id):
    sales = RecordSale.objects.filter(sales_person_id=sales_person_id)
    return JsonResponse(
        {"sales": sales},
        encoder=RecordSaleListEncoder,
    )
